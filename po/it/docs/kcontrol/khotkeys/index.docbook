<?xml version="1.0" ?>
<!DOCTYPE article PUBLIC "-//KDE//DTD DocBook XML V4.5-Based Variant V1.1//EN"
"dtd/kdedbx45.dtd" [
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % Italian "INCLUDE">
]>
<article id="khotkeys" lang="&language;">
<title
>Scorciatoie personalizzate</title>

<articleinfo>
<authorgroup>
<author
><firstname
>Subhashish</firstname
> <surname
>Pradhan</surname
></author>
<author
>&TC.Hollingsworth; &TC.Hollingsworth.mail;</author>
<othercredit role="translator"
><firstname
>Paolo</firstname
><surname
>Zamponi</surname
><affiliation
><address
><email
>zapaolo@email.it</email
></address
></affiliation
><contrib
>Traduzione del documento</contrib
></othercredit
> 
</authorgroup>

<copyright>
<year
>2011</year>
<holder
>Subhashish Pradhan</holder>
</copyright>
<copyright>
<year
>2012</year>
<holder
>&TC.Hollingsworth;</holder>
</copyright>
<legalnotice
>&FDLNotice;</legalnotice>

<date
>21/12/2021</date>
<releaseinfo
>Plasma 5.24</releaseinfo>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>Impostazioni di sistema</keyword>
<keyword
>scorciatoie</keyword>
<keyword
>tasti scorciatoia</keyword>
<keyword
>Scorciatoie personalizzate</keyword>
<keyword
>khotkeys</keyword>
</keywordset>
</articleinfo>

<sect1 id="intro">
<title
>Introduzione</title>

<para
>Il modulo <application
>Scorciatoie personalizzate</application
> delle &systemsettings; ti permette di personalizzare le scorciatoie di tastiera ed i gesti del mouse. Puoi usarli per controllare le applicazioni, per avviare comandi specifici e per altro ancora.</para>

</sect1>


<sect1 id="manage">
<title
>Gestire le scorciatoie ed i gruppi</title>

<sect2 id="manage-add-group">
<title
>Aggiungere i gruppi</title>

<para
>Il modulo ti permette di ordinare le scorciatoie correlate fra loro in gruppi. Per esempio, se hai delle scorciatoie relative al tuo lettore musicale potresti creare un gruppo chiamato <replaceable
>Lettore musicale</replaceable
>. </para>

<para
>Per aggiungere un nuovo gruppo fai clic sul pulsante <guibutton
>Modifica</guibutton
> sotto al pannello di sinistra, e seleziona l'opzione <guimenuitem
>Nuovo gruppo</guimenuitem
>.</para>

</sect2>

<sect2 id="manage-add-shortcut">
<title
>Aggiungere le scorciatoie</title>

<para
>Per aggiungere le scorciatoie fai clic sul pulsante <guibutton
>Modifica</guibutton
> sotto al pannello di sinistra, e seleziona l'opzione <guimenuitem
>Nuovo</guimenuitem
>.</para>

<para
>Il primo menu che appare ti permette di scegliere il tipo di attivazione. Sono disponibili le seguenti opzioni:</para>

<variablelist>

<varlistentry id="manage-add-shortcut-global">
<term
><guisubmenu
>Scorciatoia globale</guisubmenu
></term>
<listitem
><para
>Ci sono delle scorciatoie di tastiera che saranno riconosciute ovunque, fintanto che è in esecuzione uno spazio di lavoro &kde; &plasma;.</para
></listitem>
</varlistentry>

<varlistentry id="manage-add-shortcut-window-action">
<term
><guisubmenu
>Azione finestra</guisubmenu
></term>
<listitem
><para
>Le azioni finestra sono attivatori che scattano quando si verifica qualcosa in una particolare finestra, ad esempio quando appare, quando ottiene il fuoco o quando viene chiusa.</para>
</listitem>
</varlistentry>

<varlistentry id="manage-add-shortcut-mouse-gesture">
<term
><guisubmenu
>Azione gesto del mouse</guisubmenu
></term>
<listitem
><para
>Un'azione gesto del mouse viene attivata quando viene eseguito un particolare movimento col mouse (o con il touchpad, o sul touchscreen).</para
></listitem>
</varlistentry>

</variablelist>

<para
>Una volta che hai selezionato il tipo di attivazione ti appare un altro sotto-menu, che ti permette di scegliere il tipo di azione. Sono disponibili i seguenti tipi:</para>

<variablelist>

<varlistentry id="manage-add-shortcut-command">
<term
><guimenuitem
>Comando/URL</guimenuitem
></term>
<listitem
><para
>Questa azione eseguirà un comando o aprirà un &URL; quando la scorciatoia viene attivata.</para
></listitem>
</varlistentry>

<varlistentry id="manage-add-shortcut-dbus">
<term
><guimenuitem
>Comando DBus</guimenuitem
></term>
<listitem
><para
>Questa azione chiamerà un metodo di &DBus; in un'applicazione in esecuzione o in un demone di sistema. Per ulteriori informazioni, vedi l'<ulink url="https://develop.kde.org/docs/d-bus/introduction_to_dbus/"
>introduzione a &DBus; nella piattaforma degli sviluppatori di &kde;</ulink
>.</para
></listitem>
</varlistentry>

<varlistentry id="manage-add-shortcut-keyboard-input">
<term
><guimenuitem
>Invia immissione da tastiera</guimenuitem
></term>
<listitem
><para
>Questa azione invierà l'immissione da tastiera all'applicazione attualmente in esecuzione, proprio come se l'avessi digitata nell'applicazione tu stesso.</para
></listitem>
</varlistentry>

</variablelist>

<para
>Una volta che hai selezionato il tipo di azione puoi modificare la scorciatoia a tuo piacimento, vedi <xref linkend="shortcuts"/> per maggiori informazioni.</para>

</sect2>

<sect2 id="manage-delete">
<title
>Eliminare le scorciatoie ed i gruppi</title>

<para
>Per eliminare una scorciatoia o un gruppo selezionalo, quindi fai clic sul pulsante <guibutton
>Modifica</guibutton
> sotto al pannello di sinistra, poi seleziona l'opzione <guimenuitem
>Elimina</guimenuitem
>.</para>

</sect2>

<sect2 id="manage-export">
<title
>Esportare i gruppi</title>

<para
>Puoi esportare un gruppo; in questo modo puoi salvare le scorciatoie contenute al suo interno, per usarle in un altro computer o per effettuare un backup.</para>

<para
>Per esportare un gruppo selezionalo, quindi fai clic sul pulsante<guibutton
>Modifica</guibutton
> sotto al pannello di sinistra, quindi seleziona l'opzione <guimenuitem
>Esporta gruppo...</guimenuitem
>; questa apre una nuova finestra, che ti lascia definire le opzioni del gruppo esportato. Sono disponibili le seguenti opzioni:</para>

<variablelist>

<varlistentry id="manage-export-actions">
<term
><guilabel
>Esporta azioni</guilabel
></term>
<listitem
><para
>Ti permette di selezionare lo stato in cui saranno le opzioni quando saranno in seguito esportate. Seleziona <guilabel
>Stato corrente</guilabel
> per mantenere il loro stato attuale, <guilabel
>Abilitato</guilabel
> per assicurarti che siano tutte abilitate, oppure <guilabel
>Disabilitato</guilabel
> per assicurarti invece che siano tutte disabilitate. </para
></listitem>
</varlistentry>

<varlistentry id="manage-id">
<term
><guilabel
>Id</guilabel
></term>
<listitem
><para
>Qui puoi inserire un testo per identificare il gruppo. Se il gruppo è incluso in modo predefinito, questo testo può essere riempito in modo predefinito.</para
></listitem>
</varlistentry>

<varlistentry id="manage-merge">
<term
><guilabel
>Consenti fusione</guilabel
></term>
<listitem
><para
>Definisce cosa accade se il gruppo esiste già nel sistema in cui viene importato il gruppo esportato. Se abilitato, tutte le nuove azioni verranno aggiunte al gruppo del sistema finale, mentre le azioni con lo stesso nome ma con una configurazione diversa saranno aggiornate secondo il file importato. Se disabilitato, il modulo si rifiuterà di importare il file. </para
></listitem>
</varlistentry>

<varlistentry id="manage-filename">
<term
><guilabel
>Nome del file</guilabel
></term>
<listitem
><para
>Qui puoi inserire il nome del file in cui vuoi esportare le scorciatoie. Puoi anche selezionare il pulsante <inlinemediaobject
><imageobject
><imagedata fileref="document-open.png" format="PNG"/></imageobject
></inlinemediaobject
> alla destra della casella di testo per aprire la finestra File e selezionare un file da qui. </para>

<tip
><para
>I file esportati usano per impostazione predefinita l'estensione <literal role="extension"
>.khotkeys</literal
>.</para
></tip>
</listitem>
</varlistentry>

</variablelist>

<screenshot id="screenshot-manage-export">
<screeninfo
>Esportare un gruppo</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="manage-export.png" format="PNG"/></imageobject>
<textobject
><phrase
>La finestra Esporta gruppo.</phrase
></textobject>
<caption
><para
>Esportare un gruppo di scorciatoie.</para
></caption>
</mediaobject>
</screenshot>

</sect2>

<sect2 id="manage-import">
<title
>Importare i gruppi</title>

<para
>Per importare un gruppo fai clic sul pulsante <guibutton
>Modifica</guibutton
> sotto al pannello di sinistra, e seleziona <guimenuitem
>Importa...</guimenuitem
>: si aprirà una finestra di selezione dei file, che ti permette di localizzare un file creato in precedenza con la funzione Esporta.</para>

</sect2>

</sect1>


<sect1 id="groups">
<title
>Modificare i gruppi</title>

<para
>Nella sezione sinistra della finestra le scorciatoie predefinite sono catalogate in gruppi, che possono essere espansi facendo clic sulla freccia vicino ad essi, in modo da svelare le scorciatoie.</para>

<para
>Quando fai clic su un gruppo ti vengono presentate due schede per configurarlo: <guilabel
>Commento</guilabel
> ti permette di salvare delle note sul gruppo, e non è effettivamente usato dal sistema, mentre la scheda <guilabel
>Condizioni</guilabel
> ti permette di restringere le finestre su cui lavora il gruppo di scorciatoie.</para>

<screenshot id="screenshot-groups-comment">
<screeninfo
>La scheda Commento</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="groups-comment.png" format="PNG"/></imageobject>
<textobject
><phrase
>La scheda Commento in un gruppo.</phrase
></textobject>
<caption
><para
>Modificare un commento di un gruppo.</para
></caption>
</mediaobject>
</screenshot>

<para
>Le condizioni sono visualizzate in un albero, sul cui livello più altro c'è <guilabel
>e</guilabel
>: per attivare scorciatoie nel gruppo devono essere soddisfatte tutte le condizioni sotto <guilabel
>e</guilabel
>.</para>

<para
>Puoi aggiungere delle condizioni aggiuntive, facendo clic sulla lista a cascata <guibutton
>Nuova</guibutton
> alla destra dell'albero delle condizioni; i tipi di gruppi includono il summenzionato <guimenuitem
>e</guimenuitem
>, <guimenuitem
>o</guimenuitem
>, nel quale deve essere soddisfatto solo un membro del gruppo, oppure <guimenuitem
>non</guimenuitem
>, nel quale l'inverso di tutti i membri attiverà le scorciatoie nel gruppo.</para>

<para
>Per aggiungere una definizione di finestra alla lista premi il pulsante<guibutton
>Nuova</guibutton
>: puoi selezionare <guimenuitem
>Finestra attiva...</guimenuitem
> se vuoi che la scorciatoia venga attivata solo se la finestra specificata ha il fuoco, o anche <guimenuitem
>Finestra esistente...</guimenuitem
> se vuoi che la scorciatoia venga attivata finché la finestra è aperta, sia che tu la stia usando o meno. Selezionando entrambe queste opzioni si aprirà una finestra, dalla quale potrai modificare le definizioni di finestra.</para>

<para
>Fai clic sul pulsante <guibutton
>Modifica...</guibutton
> per modificare un insieme di definizioni di finestra esistente: si aprirà una finestra con l'editor delle definizioni di finestra. Per ulteriori informazioni, vedi <xref linkend="windows"/>.</para>

<para
>Fai clic sul pulsante <guibutton
>Elimina</guibutton
> per rimuovere una definizione di finestra dalla lista delle definizioni.</para>

</sect1>


<sect1 id="shortcuts">
<title
>Modificare le scorciatoie</title>

<para
>Nella sezione sinistra della finestra le scorciatoie predefinite sono catalogate in gruppi, che possono essere espansi facendo clic sulla freccia vicino ad essi, in modo da svelare le scorciatoie.</para>

<para
>Come impostazione predefinita sono installati i gruppi <guilabel
>KMenuEdit</guilabel
>, <guilabel
>Gesti di Konqueror</guilabel
> ed <guilabel
>Esempi</guilabel
>. Le applicazioni possono fornire delle scorciatoie aggiuntive, &eg; &spectacle; aggiunge un gruppo <guilabel
>Schermate</guilabel
>. Questo svela, quando espanso, numerose configurazioni di scorciatoie, come <guilabel
>Avvia lo strumento per le schermate</guilabel
> che, quando selezionato, mostra una sezione sulla destra con tre schede:</para>

<sect2 id="shortcuts-comment">
<title
>La scheda Commento</title>

<para
>La scheda <guilabel
>Commento</guilabel
> ti permette di descrivere come usare la scorciatoia, che cosa fa, o qualsiasi altra cosa che potresti voler includere.</para>

</sect2>

<sect2 id="shortcuts-trigger">
<title
>La scheda Attivazione</title>
<para
>La scheda <guilabel
>Attivazione</guilabel
> contiene la configurazione di attivazione, che dipende dal tipo di attivazione specificata:</para>

<variablelist>

<varlistentry id="shortcuts-trigger-keyboard">
<term
>Scorciatoia globale</term>
<listitem>

<para
>Per modificare una scorciatoia da tastiera fai clic sul pulsante contenente una chiave inglese, quindi inserisci la scorciatoia da tastiera desiderata. Per cancellarne una fai clic sul pulsante con l'icona <inlinemediaobject
><imageobject
><imagedata fileref="oxygen-22x22-edit-clear-locationbar-rtl.png" format="PNG"/></imageobject
></inlinemediaobject
> alla destra del pulsante per cambiare la scorciatoia.</para>

<screenshot id="screenshot-shortcuts-trigger-keyboard">
<screeninfo
>Modificare un'attivazione da tastiera</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="shortcuts-trigger-keyboard.png" format="PNG"/></imageobject>
<textobject
><phrase
>La scheda Attivazione per un'attivazione da tastiera.</phrase
></textobject>
<caption
><para
>Modificare un'attivazione per un'attivazione da tastiera.</para
></caption>
</mediaobject>
</screenshot>

</listitem>
</varlistentry>

<varlistentry id="shortcuts-trigger-window">
<term
>Azione finestra</term>
<listitem>

<para
>Le azioni finestra contengono le seguenti opzioni:</para>

<variablelist>

<varlistentry id="shortcuts-trigger-window-trigger">
<term
><guilabel
>Attiva quando</guilabel
></term>
<listitem>

<para
>Configura la particolare azione finestra che deve succedere quando la scorciatoia viene attivata. Sono disponibili le seguenti opzioni:</para>

<itemizedlist>

<listitem
><para
><guilabel
>Appare la finestra</guilabel
> - attivata quando viene aperta una finestra.</para
></listitem>

<listitem
><para
><guilabel
>Scompare la finestra</guilabel
> - attivata quando viene chiusa una finestra.</para
></listitem>

<listitem
><para
><guilabel
>La finestra riceve il fuoco</guilabel
> - attivata quando passi ad una finestra.</para
></listitem>

<listitem
><para
><guilabel
>La finestra perde il fuoco</guilabel
> - attivata quando lasci una finestra.</para
></listitem>

</itemizedlist>

</listitem>
</varlistentry>

<varlistentry id="shortcuts-trigger-window-window">
<term
><guilabel
>Finestra</guilabel
></term>
<listitem
><para
>È dove definisci l'attuale finestra o le finestre a cui si applica l'attivazione; vedi <xref linkend="windows"/> per maggiori informazioni.</para
></listitem>
</varlistentry>

</variablelist>

</listitem>
</varlistentry>

<varlistentry id="shortcuts-trigger-mouse">
<term
>Gesto del mouse</term>
<listitem>

<para
>Un gesto del mouse può essere cambiato facendo clic sul pulsante <guibutton
>Modifica</guibutton
> sotto all'area che lo visualizza. Ciò aprirà una finestra: tieni premuto il &LMB; e disegna il gesto del mouse desiderato nell'area apposita,che sarà salvato quando rilascerai il tasto.</para>

<screenshot id="screenshot-shortcuts-trigger-mouse">
<screeninfo
>Modificare un'attivazione da gesto del mouse</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="shortcuts-trigger-mouse.png" format="PNG"/></imageobject>
<textobject
><phrase
>La scheda Attivazione per una scorciatoia da gesto del mouse.</phrase
></textobject>
<caption
><para
>Modificare un'attivazione per un gesto del mouse.</para
></caption>
</mediaobject>
</screenshot>

</listitem>
</varlistentry>

</variablelist>

</sect2>

<sect2 id="shortcuts-action">
<title
>La scheda Azione</title>

<para
>La scheda <guilabel
>Azione</guilabel
> è dove configuri l'azione che sarà eseguita quando la scorciatoia viene attivata. Ci sono vari tipi di azioni, che hanno diverse opzioni di configurazione:</para>

<variablelist>

<varlistentry id="shortcuts-action-command">
<term
>Comando/&URL;</term>
<listitem>

<para
>Quando usi come attivazione un comando o un &URL; ti viene fornita una casella di testo dove puoi inserire il comando da eseguire o l'&URL; da aprire quando viene attivata la scorciatoia. Puoi anche fare clic sul pulsante <inlinemediaobject
><imageobject
><imagedata fileref="document-open.png" format="PNG"/></imageobject
></inlinemediaobject
> alla destra della casella di testo per aprire una finestra di selezione dei file da cui puoi selezionare un file sul tuo sistema locale o remoto.</para>

<screenshot id="screenshot-shortcuts-action-command">
<screeninfo
>Modificare un'azione del comando</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="shortcuts-action-command.png" format="PNG"/></imageobject>
<textobject
><phrase
>La scheda Azione di un comando</phrase
></textobject>
<caption
><para
>Modificare un'azione del comando.</para
></caption>
</mediaobject>
</screenshot>

</listitem>
</varlistentry>

<varlistentry id="shortcuts-action-dbus">
<term
>Comando &DBus;</term>
<listitem>

<screenshot id="screenshot-shortcuts-action-dbus">
<screeninfo
>Modificare un'azione di &DBus;</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="shortcuts-action-dbus.png" format="PNG"/></imageobject>
<textobject
><phrase
>La scheda Azione di un comando di &DBus;.</phrase
></textobject>
<caption
><para
>Modificare un'azione di &DBus;.</para
></caption>
</mediaobject>
</screenshot>

<para
>Sono fornite le seguenti opzioni, che ti permettono di specificare un metodo di &DBus; da eseguire:</para>

<variablelist>

<varlistentry id="shortcuts-action-dbus-application">
<term
><guilabel
>Applicazione remota:</guilabel
></term>
<listitem
><para
>Il nome del servizio dell'applicazione remota su cui il metodo viene eseguito, per esempio <userinput
>org.kde.spectacle</userinput
> se lo vuoi eseguire sull'utilità per schermate &spectacle;.</para
></listitem>
</varlistentry>

<varlistentry id="shortcuts-action-dbus-object">
<term
><guilabel
>Oggetto remoto:</guilabel
></term>
<listitem
><para
>Il percorso dell'oggetto remoto su cui eseguire il metodo, per esempio <userinput
>/</userinput
> per le azioni sulla schermata usando &spectacle;, oppure <userinput
>/Document/1</userinput
> se lo vuoi eseguire sul primo documento aperto in &kate;.</para
></listitem>
</varlistentry>

<varlistentry id="shortcuts-action-dbus-function">
<term
><guilabel
>Funzione:</guilabel
></term>
<listitem
><para
>Il nome del metodo &DBus; da chiamare, per esempio <userinput
>Fullscreen</userinput
> se vuoi catturare una schermata a tutto schermo, oppure <userinput
>print</userinput
> se vuoi stampare il documento.</para
></listitem>
</varlistentry>

<varlistentry id="shortcuts-action-dbus-arguments">
<term
><guilabel
>Argomenti:</guilabel
></term>
<listitem
><para
>Inserire ulteriori argomenti per il metodo di &DBus; da chiamare.</para
></listitem>
</varlistentry>

<varlistentry id="shortcuts-action-dbus-call">
<term
><guilabel
>Chiamata</guilabel
></term>
<listitem
><para
>Usa questo pulsante per verificare che l'azione funzioni come previsto.</para
></listitem>
</varlistentry>

<varlistentry id="shortcuts-action-dbus-launch-dbus-browser">
<term
><guilabel
>Esegui browser D-Bus</guilabel
></term>
<listitem
><para
>Esegue l'applicazione <application
>QDBusViewer</application
>, quindi sfoglia i metodi di &DBus; e gli argomenti di un'applicazione in esecuzione.</para
></listitem>
</varlistentry>

</variablelist>

<para
>Per ulteriori informazioni, vedi l'<ulink url="https://develop.kde.org/docs/d-bus/introduction_to_dbus/"
>introduzione a &DBus; nella piattaforma degli sviluppatori di &kde;</ulink
>.</para>

</listitem>
</varlistentry>

<varlistentry id="shortcuts-action-keyboard">
<term
>Invia immissione da tastiera</term>
<listitem>

<para
>In alto nella scheda <guilabel
>Azione</guilabel
> c'è una grossa voce di testo: qui puoi inserire le combinazioni di tasti che vuoi inviare quando la scorciatoia viene attivata.</para>

<para
>Molti tasti contengono un singolo carattere; per inserirli inserisci semplicemente quel carattere. Per esempio, per digitare una <quote
>A</quote
>, inserisci solo <userinput
>A</userinput
>. Altri hanno invece un nome più lungo, e puoi usare anche quelli; per esempio, per digitare il tasto &Alt;, scrivi semplicemente <userinput
>Alt</userinput
>. </para>

<para
>Le combinazioni di tasti dovrebbero essere separate dai due punti (<userinput
>:</userinput
>); per esempio, per scrivere <quote
>ciao</quote
> scrivi <userinput
>C:I:A:O</userinput
>.</para>

<para
>I tasti che devono essere premuti contemporaneamente devono essere separati dal segno più: per esempio, per premere <keycombo action="simul"
>&Ctrl;<keycap
>C</keycap
></keycombo
> digita <userinput
>Ctrl+C</userinput
>.</para>

<tip>
<para
>Ricorda, qui devi inserire le combinazioni di tasti esattamente come faresti sulla tastiera. Per fare le lettere maiuscole devi digitare il tasto &Shift;; per esempio, per scrivere <quote
>Ciao</quote
> inserisci <userinput
>Shift+C:I:A:O</userinput
>.</para>

<para
>Questo si applica anche ai caratteri speciali. Per esempio, per digitare il simbolo ! su una tastiera con mappatura italiana, inserisci <userinput
>Shift+1</userinput
>. </para>
</tip>

<warning>
<para
>L'azione che viene eseguita dipende dalla mappatura della tastiera attualmente selezionata: se la cambi ed attivi una scorciatoia, puoi avere delle conseguenze indesiderate.</para>
</warning>

<para
>Sotto alla casella di testo per l'immissione della combinazione di tasti puoi selezionare a quale finestra questa sarà diretta. Puoi scegliere una delle seguenti opzioni:</para>

<itemizedlist>

<listitem
><para
><guilabel
>Finestra attiva</guilabel
> - la finestra attualmente aperta.</para
></listitem>

<listitem
><para
><guilabel
>Finestra specifica</guilabel
> - la finestra che descrivi usando la forma sotto. Per ulteriori informazioni sulla descrizione delle finestre vedi <xref linkend="windows"/>.</para
></listitem>

<listitem
><para
><guilabel
>Finestra dell'azione</guilabel
> - quando si usa il tipo di attivazione a finestra dell'azione, inserisci la combinazione nella finestra che attiva la scorciatoia.</para
></listitem>

</itemizedlist>

<screenshot id="screenshot-shortcuts-action-keyboard">
<screeninfo
>Modificare un'azione di inserimento da tastiera</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="shortcuts-action-keyboard.png" format="PNG"/></imageobject>
<textobject
><phrase
>La scheda Azione per l'immissione da tastiera.</phrase
></textobject>
<caption
><para
>Modificare l'azione di inserimento da tastiera per una scorciatoia.</para
></caption>
</mediaobject>
</screenshot>

</listitem>
</varlistentry>

</variablelist>

</sect2>

</sect1>


<sect1 id="windows">
<title
>Definire le finestre</title>

<para
>Molte aree del modulo ti permettono di definire una lista di finestre. Queste utilizzano tutte la stessa interfaccia, che contiene le seguenti opzioni:</para>

<variablelist>

<varlistentry id="windows-comment">
<term
><guilabel
>Commento:</guilabel
></term>
<listitem
><para
>È una casella di testo puramente informativa, che puoi usare per spiegare a cosa si applica l'attivazione, o per salvare altre informazioni utili. Non viene usato dal sistema.</para
></listitem>
</varlistentry>

<varlistentry id="windows-list">
<term
>Elenco di finestre</term>
<listitem
><para
>Sotto il riquadro <guilabel
>Commento</guilabel
> a sinistra c'è l'elenco di tutte le definizioni di finestra attualmente presenti nell'attivatore; fai semplicemente clic su una di esse per eseguirci sopra un'operazione.</para
></listitem>
</varlistentry>

<varlistentry id="windows-edit">
<term
><guibutton
>Modifica...</guibutton
></term>
<listitem>

<para
>Premi questo pulsante per modificare la definizione di finestra attualmente selezionata; si apre una nuova finestra che ti permette di modificarla. Questa finestra contiene le seguenti opzioni:</para>

<variablelist>

<varlistentry id="windows-edit-data">
<term
><guilabel
>Dati finestra</guilabel
></term>
<listitem>

<para
>È dove descrivi la finestra su cui dovresti applicare l'attivazione.</para>

<para
>In alto c'è un campo <guilabel
>Commento:</guilabel
> che è informativo come quello nella scheda principale <guilabel
>Attivazione</guilabel
>.</para>

<para
>Sono disponibili tre caratteristiche della finestra:</para>

<itemizedlist>

<listitem
><para
><guilabel
>Titolo della finestra:</guilabel
> - il titolo che appare in alto nella finestra.</para
></listitem>

<listitem
><para
><guilabel
>Classe della finestra:</guilabel
> - di solito è il nome dell'applicazione.</para
></listitem>

<listitem
><para
><guilabel
>Ruolo della finestra:</guilabel
> - di solito è il nome della classe &Qt; che costituisce la finestra.</para
></listitem>

</itemizedlist>

<para
>Ogni caratteristica della finestra fornisce un menu a cascata ed una casella di testo al di sotto di essa: inserisci il valore che vuoi provare per la finestra nella casella di testo, e seleziona dal menu a cascata il tipo di prova vorresti fare. Puoi usare <guilabel
>è</guilabel
> per richiedere una corrispondenza esatta, <guilabel
>contiene</guilabel
> per richiedere che il testo inserito sia all'interno del valore totale, oppure <guilabel
>corrisponde all'espressione regolare</guilabel
> per usare un'espressione regolare per definire una corrispondenza. Puoi anche eseguire l'inverso di tutte queste operazioni, o anche <guilabel
>non è importante</guilabel
> se non vuoi che venga esaminata quella particolare caratteristica.</para>

<para
>La maniera più semplice per riempire queste informazioni è aprire la finestra desiderata, far clic sul pulsante <guibutton
>Rilevamento automatico</guibutton
> in basso a questa sezione, ed infine fare clic sull'attivazione di finestra desiderato. Tutte queste caratteristiche saranno popolate con le informazioni di quella finestra, e se necessario potrai regolare le impostazioni.</para>

</listitem>
</varlistentry>

<varlistentry id="windows-edit-types">
<term
><guilabel
>Tipi di finestra</guilabel
></term>
<listitem>

<para
>Questo consente di limitare la corrispondenza ad un particolare tipo di finestra. Sono disponibili le seguenti opzioni:</para>

<itemizedlist>

<listitem
><para
><guilabel
>Normale</guilabel
> - una finestra dell'applicazione regolare.</para
></listitem>

<listitem
><para
><guilabel
>Desktop</guilabel
> - il desktop principale è in realtà una finestra speciale in sé.</para
></listitem>

<listitem
><para
><guilabel
>Finestra di dialogo</guilabel
> - una piccola finestra che è parte di una normale applicazione, come un riquadro informativo o una finestra di configurazione.</para
></listitem>

<listitem
><para
><guilabel
>Area d'aggancio</guilabel
> - una piccola finestra che può essere attaccata o staccata dalla finestra principale dell'applicazione. </para
></listitem>

</itemizedlist>

</listitem>
</varlistentry>

</variablelist>

</listitem>
</varlistentry>

<varlistentry id="windows-new">
<term
><guibutton
>Nuovo...</guibutton
></term>
<listitem
><para
>Crea una nuova definizione di finestra. Questo apre la finestra modifica descritta qui sotto.</para
></listitem>
</varlistentry>

<varlistentry id="windows-duplicate">
<term
><guibutton
>Duplica...</guibutton
></term>
<listitem
><para
>Crea una nuova definizione di finestra che ha esattamente le stesse specifiche di quella attualmente selezionata. Si aprirà la finestra di modifica descritta qui sotto, in modo che tu possa fare ulteriori modifiche.</para
></listitem>
</varlistentry>

<varlistentry id="windows-delete">
<term
><guibutton
>Elimina</guibutton
></term>
<listitem
><para
>Rimuove le definizioni di finestra attualmente selezionate.</para
></listitem>
</varlistentry>

</variablelist>

</sect1>


<sect1 id="settings">
<title
>Impostazioni</title>

<para
>Quando entri per la prima volta nel modulo, o quando fai clic sul pulsante <guibutton
>Impostazioni</guibutton
> sotto al pannello di sinistra, trovi molte opzioni di configurazione in quello di destra:</para>

<variablelist>

<varlistentry id="settings-start-daemon">
<term
><guilabel
>Esegui il demone delle azioni di immissione all'accesso</guilabel
></term>
<listitem
><para
>Configura se attivare o no l'applicazione in background che controlla le scorciatoie di tastiera ed attiva le azioni configurate. È attivata per impostazione predefinita.</para
></listitem>
</varlistentry>

<varlistentry id="settings-gestures">
<term
><guilabel
>Gesti</guilabel
></term>
<listitem>
<para
>Seleziona questa casella per abilitare i gesti del mouse.</para>

<para
>Ci sono due azioni specifiche per i gesti del mouse:</para>

<variablelist>

<varlistentry id="settings-gestures-timeout">
<term
><guilabel
>Tempo massimo:</guilabel
></term>
<listitem
><para
>Specifica il massimo intervallo di tempo in millisecondi in cui vengono monitorati i gesti del mouse per essere riconosciuti dal sistema. </para
></listitem>
</varlistentry>

<varlistentry id="settings-gestures-mouse-button">
<term
><guilabel
>Pulsante del mouse:</guilabel
></term>
<listitem
><para
>Specifica il pulsante da usare per i gesti del mouse. Se il tuo mouse ha altri pulsanti puoi invece usare quelli al loro posto.</para>

<note>
<para
>Il pulsante sinistro non è disponibile, quindi i gesti del mouse non interferiscono con le normali operazioni nel tuo sistema.</para>
</note>
</listitem>
</varlistentry>

</variablelist>
</listitem>
</varlistentry>

</variablelist>

<screenshot id="screenshot-settings">
<screeninfo
>Impostazioni</screeninfo>
<mediaobject>
<imageobject
><imagedata fileref="settings.png" format="PNG"/></imageobject>
<textobject
><phrase
>La schermata Impostazioni.</phrase
></textobject>
<caption
><para
>Modificare le impostazioni delle scorciatoie personalizzate.</para
></caption>
</mediaobject>
</screenshot>

</sect1>


<sect1 id="credits">
<title
>Riconoscimenti e licenza</title>

<para
>Ringraziamenti speciali vanno al partecipante del Google Code-In 2011, Subhashish Pradhan, per aver scritto la gran parte di questo articolo.</para>

<para
>Traduzione a cura di Paolo Zamponi<email
>zapaolo@email.it</email
></para
> &underFDL; &underGPL; </sect1>

</article>

<!--
Local Variables:
mode: xml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:

vim:tabstop=2:shiftwidth=2:expandtab
kate: space-indent on; indent-width 2; tab-width 2; indent-mode none;
-->
